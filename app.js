const http = require("http");
const fs = require("fs");

const id = {
  uuid: "14d96bb1-5d53-472f-a96e-b3a1fa82addd",
};

const server = http.createServer((req, res) => {
  let path = "Response";
  let statusCode = (delayTime = "");

  if (req.url != "/favicon.ico") {
    statusCode = delayTime = req.url.split("/")[2];
  }

  if (req.url === "/html") {
    path += req.url + ".html";
    fs.readFile(path, (err, data) => {
      if (err) {
        console.log(err);
      } else {
        res.write(data);
        res.end();
      }
    });
  } else if (req.url === "/json") {
    path += req.url + ".json";
    fs.readFile(path, (err, data) => {
      if (err) {
        console.log(err);
      } else {
        res.write(data);
        res.end();
      }
    });
  } else if (req.url === "/uuid") {
    res.write(JSON.stringify(id));
    res.end();
  } else if (req.url === `/status/${statusCode}`) {
    res.writeHead(statusCode, http.STATUS_CODES[statusCode]);
    res.write(
      `<h1>Status ${statusCode} :- ${http.STATUS_CODES[statusCode]}</h1>`
    );
    res.end();
  } else if (req.url === `/delay/${delayTime}`) {
    setTimeout(() => {
      res.writeHead(200);
      res.write(`Response in ${delayTime}s`);
      res.end();
    }, delayTime * 1000);
  } else {
    res.write("<h1 >Home Page</h1>");
    res.end();
  }
});

server.listen(8000, () => {
  console.log("Server is running on port 8000...");
});
